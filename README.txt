
Module: AudienceScience

Description
===========
This module integrates AudienceScience behavioural targeting platform.
Enables publishers to deliver relevant advertising to advertisers,
based on users’ online behaviour into Drupal.

Installation
============
Copy the module directory into your Drupal:
/sites/all/modules directory.

Configuraton
===================================================================
Visit /admin/config/system/audiencescience

Enter the advertiser code provided to you by AudienceScience.

Enter the Ad tag code provided by AudienceScience.

Check the box to enable AudienceScience.
