/*
 * The code below will take the value of the segQS variable
 * which is defined in the audiencesceince adtag code entered
 * on the admin form and add it to the DART ad tag before the
 * ord number.
 */
(function ($) {
  $(document).bind('dart_tag_render', function(event, tag) {
    if (typeof segQS != 'undefined') {
      tag = tag.replace('ord=', segQS + 'ord=');
    }
    return tag;
  });
}(jQuery));
